const express = require('express');
const bodyParser = require('body-parser');

const app = express();
app.use(bodyParser.json());

let products = [
    {
        id: 0,
        productName: 'Samsung Galaxy S9',
        price: 4000
    },
    {
        id: 1,
        productName: 'Iphone XR',
        price: 3999
    }
];

app.get('/get-all', (req, res) => {
    res.status(200).send(products);
})

app.post('/add', (req, res) => {
    if(req.body.productName && req.body.price){
        let product = {
            id: products.length,
            productName: req.body.productName,
            price: req.body.price
        };
        products.push(product);
        res.status(200).send(product);
    } else {
        res.status(500).send('Error!');
    }
});

//tema: put
app.put('/products/:id/update', (req, res) =>{
   const id = req.params.id;
   let updated = false;
   products.forEach((product) =>{
      if(product.id == id){
        
          product.productName = req.body.productName;
          product.price = req.body.price;
        
            updated = true;
      } 
   });
   if(updated){
    res.status(200).send(`Product ${id} updated!`);    
   } else {
       res.status(404).send(`Could not find resource with id ${id}`);
   }
   
});
//tema: delete

app.delete('/products/delete', (req, res) =>{
   var name = req.body.productName;
   let deleted=false;
   var index=products.indexOf(name);
   if(index!=-1){
        
        products.splice(index,1);
            deleted = true;
      } 
 
   if(deleted){
    res.status(200).send(`Product deleted!`);    
   } else {
       res.status(404).send(`Could not find that product`);
   }
   
});


app.listen(8080, () => {
    console.log('Server started on port 8080...');
});